VERSIONS = mcpatcher-legacy.json
BASEURL = https://bitbucket.org/prupe/mcpatcher-legacy/downloads/
MCPATCHER = lib/mcpatcher-4.2.2.jar
MAIN = com.prupe.mcpatcher.Main
RUN = java -cp out/production/mcpatcher-legacy:$(MCPATCHER) $(MAIN)

JAR2D = out/artifacts/mods2
JAR3D = out/artifacts/mods3
JAR2 = $(JAR2D)/mods2.jar
JAR3 = $(JAR3D)/mods3.jar
JAR2V = mcpatcher-legacy-`$(RUN) -getversion mods2`.jar
JAR3V = mcpatcher-legacy-`$(RUN) -getversion mods3`.jar

.PHONY: build clean updmcpatcher restorejars versions release2 release3 release

default:

build:
	ant

$(JAR2): build

$(JAR3): build

clean:
	rm -rf out mcpatcher-legacy-*.jar
	git checkout -- $(VERSIONS)

updmcpatcher:
	cp ../mcpatcher/out/artifacts/mcpatcher/mcpatcher.jar $(MCPATCHER)

restorejars:
	mkdir -p $(JAR2D) $(JAR3D)
	wget -O $(JAR2) $(BASEURL)$(JAR2V)
	wget -O $(JAR3) $(BASEURL)$(JAR3V)

versions:
	$(RUN) -export $(VERSIONS)

release2: $(JAR2) versions
	cp $(JAR2) $(JAR2V)

release3: $(JAR3) versions
	cp $(JAR3) $(JAR3V)

release: release2 release3
